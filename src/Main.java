import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Main {
    static Scanner scanner = new Scanner(System.in);
    public static String name = "";

    public static void main(String[] args) {
        MyObj myobj = new MyObj();

        boolean exit = false;
        int sel;
        //run main function
        System.out.println("歡迎來到朝陽銀行ATM！");
        System.out.println("您的大名是？");

        name = scanner.next();
        System.out.println(name + "，您好！");

        do{
            System.out.println("請選擇您要使用的功能(0 = 結束, 1 = 存款, 2 = 提款, 3 = 檢查餘額)：");
            sel = scanner.nextInt();
            switch (sel){
                case 0:
                    exit = true;
                    break;
                case 1:
                    myobj.Deposit();
                    break;
                case 2:
                    myobj.Withdraw();
                    break;
                case 3:
                    myobj.Check();
                    break;
                    default:
                        System.out.println("錯誤的操作，請重新輸入！");
                        break;
            }

        }while(!exit);

        System.out.println("謝謝光臨，歡迎下次再使用！");
        /* First Version
        while (true)
        {
            System.out.println("Please sel your function(0 =  Deposit, 1 =  withdraw, 2 = check, 3 = End):");
            switch (sel = scanner.nextInt())
            {
                case 0:
                    System.out.println("How much money do you want to deposit?");
                    money = scanner.next();
                    if(acc + change(money) > 30000000 && change(money) <= 0)
                        System.out.println("已超過錢包的上限值3000萬或金額有誤，操作失敗！");
                    else {
                        acc += change(money);
                        System.out.println(name + "，已收到您的" + money + "元，謝謝！");
                    }
                    break;
                case 1:
                    System.out.println("請輸入要提款的金額：");
                    money = scanner.next();
                    if(acc - change(money) < 0 && change(money) <= 0)
                    System.out.println("錢包餘額不足或金額錯誤，操作失敗！");
                    else {
                        acc -= change(money);
                        System.out.println(name + "，已成功提領" + change(money) + "元，謝謝您。");
                    }
                    break;
                case 2:
                    System.out.println("您的餘額尚有：" + change(acc) + "元");
                    break;
                case 3:
                    System.out.println("謝謝您，歡迎下次再使用！");
                    System.exit(0);
                    break;
                default:
                    System.out.println("錯誤的操作，請重新輸入！");
                    break;
            }

        }*/
 }
    static class MyObj implements BankFunc{
        String money;
        int acc = 0;

        public void Deposit() {
            System.out.println("How much money do you want to deposit?");
            money = scanner.next();
            if(acc + change(money) > 30000000 || change(money) <= 0) {
                System.out.println("已超過錢包的上限值3000萬或金額有誤，操作失敗！");
            }else {
                acc += change(money);
                System.out.println(name + "，已收到您的" + change(Integer.parseInt(money)) + "元，謝謝！");
            }
        }
        public void Withdraw(){
            System.out.println("請輸入要提款的金額：");
            money = scanner.next();
            if(acc - change(money) < 0 || change(money) <= 0) {
                System.out.println("錢包餘額不足或金額錯誤，操作失敗！");
            }    else {
                acc -= change(money);
                System.out.println(name + "，已成功提領" + change(Integer.parseInt(money)) + "元，謝謝您。");
            }
        }
        public void Check(){
            System.out.println("您的餘額尚有：" + change(acc) + "元");
        }

        public int change(String moneys)
        {
            String regEX = "[^0-9]";
            Pattern p = Pattern.compile(regEX);
            Matcher m = p.matcher(moneys);
            int res = Integer.parseInt(m.replaceAll(""));
            if(moneys.contains("萬"))
                return res * 10000;
            if(moneys.contains("千"))
                return res * 1000;

            return res;
        }

        public String change(int moneys)
        {
            if(moneys >= 10000)
                return (moneys / 10000) + "萬" + (moneys % 10000 > 0 ? change(moneys % 10000) : "");
            if(moneys >= 1000)
                return (moneys / 1000) + "千" + (moneys % 1000 > 0 ? change(moneys % 1000) : "");

            return Integer.toString(moneys);
        }
    }

    interface BankFunc{
        public void Withdraw();
        public void Deposit();
        public void Check();
    }
}
